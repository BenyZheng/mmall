package com.zby.mmall.controller.portal;

import com.zby.mmall.common.Const;
import com.zby.mmall.common.ServerResponse;
import com.zby.mmall.pojo.User;
import com.zby.mmall.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;



/**
 * @author beny
 * @Create 2020-09-16-23:53
 */

@RestController
@RequestMapping("/user")
public class UserController {

    private  final  static Logger logger = LoggerFactory.getLogger(UserController. class);

    @Autowired
    private UserService userService;

    /**
     * 登录
     * @param username
     * @param password
     * @param httpSession
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ServerResponse<User> login(@RequestParam String username , @RequestParam String password , HttpSession httpSession){
        ServerResponse<User> serverResponse = userService.login(username, password);
        //判断用户名和密码是否正确，正确则存入session
        if(serverResponse.isSuccess()){
            httpSession.setAttribute(Const.CURRENT_USER, serverResponse.getData().getUsername());
            serverResponse.getData().setPassword(StringUtils.EMPTY);
        }
        return serverResponse;
    }

    /**
     * 登出
     * @param httpSession
     * @return
     */
    @RequestMapping(value = "/logout" , method = RequestMethod.GET)
    public ServerResponse<User> logout(HttpSession httpSession){
        httpSession.removeAttribute(Const.CURRENT_USER);
        return ServerResponse.sucesss();
    }

    /**
     * 注册
     * @param user
     * @return
     */
    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    public ServerResponse<String> register(@RequestBody User user){
        ServerResponse<String> serverResponse = userService.register(user);
        return serverResponse;
    }

    /**
     * input框验证用户名和密码是否规范
     * @param type
     * @param content
     * @return
     */
    @RequestMapping(value = "/checkValid" , method = RequestMethod.GET)
    public ServerResponse<String> checkValid(@RequestParam String type , @RequestParam String content){
        return userService.checkValid(type , content);
    }

    /**
     * 获取当前登录用户信息
     * @param httpSession
     * @return
     */
    @RequestMapping(value = "/getUserInfo" , method = RequestMethod.GET)
    public ServerResponse<User> getUserInfo(HttpSession httpSession){
        String username = (String) httpSession.getAttribute(Const.CURRENT_USER);
        if(StringUtils.isNotBlank(username)){
            return userService.getUserInfo(username);
        }
        return ServerResponse.fail("用户未登录，请重新登录");
    }

    /**
     * 用户设置的问题
     * @param username
     * @return
     */
    @RequestMapping(value = "/getForgetQuestion" , method = RequestMethod.GET)
    public ServerResponse<String> getForgetQuestion(@RequestParam String username){
        return userService.getForgetQuestion(username);
    }

    /**
     * 功能描述: 检查用户忘记密码的答案是否正确，正确则返回token
     *
     * @param username
     * @param question
     * @param answer
     * @Return: com.zby.mmall.common.ServerResponse<java.lang.String>
     * @Author: zhengby001
     * @Date: 2020/9/23 18:04
     */
    @RequestMapping(value = "/checkForgetAnswer" , method = RequestMethod.GET)
    public ServerResponse<String> checkForgetAnswer(@RequestParam String username , @RequestParam String question
            , @RequestParam String answer){
        return userService.checkQuestionAnswer(username , question , answer);
    }

}
