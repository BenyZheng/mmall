package com.zby.mmall.dao;

import com.zby.mmall.pojo.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int checkUserName(@Param("username") String username);

    User login(@Param("username") String username, @Param("password") String password);

    User getUserInfo(@Param("username") String username);

    String getForgetQuestion(@Param("username") String username);

    int getForgetAnswerByUsername(@Param("username") String username,
                                  @Param("question") String question,
                                  @Param("answer") String answer);
}