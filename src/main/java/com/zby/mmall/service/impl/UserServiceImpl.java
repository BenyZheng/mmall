package com.zby.mmall.service.impl;

import com.zby.mmall.common.Const;
import com.zby.mmall.common.ServerResponse;
import com.zby.mmall.common.TokenCache;
import com.zby.mmall.dao.UserMapper;
import com.zby.mmall.pojo.User;
import com.zby.mmall.service.UserService;
import com.zby.mmall.util.MD5Util;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

/**
 * @author beny
 * @Create 2020-09-19-17:50
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public ServerResponse<User> login(String username, String password) {

        if(username == "" || password == ""){
            return ServerResponse.fail("用户名和密码不能为空");
        }

        int flag = userMapper.checkUserName(username);
        if(flag == 0){
            return ServerResponse.fail("用户名不存在");
        }
        User user = userMapper.login(username , MD5Util.MD5EncodeUtf8(password));
        if(user == null){
            return ServerResponse.fail("密码错误");
        }
        return ServerResponse.sucesss(user);
    }

    @Override
    public ServerResponse<String> register(User user){

        if(user.getUsername() == "" || user.getPassword() == ""){
            return ServerResponse.fail("用户名和密码不能为空");
        }

        //1、判断用户名是否存在
        int flag = userMapper.checkUserName(user.getUsername());
        if(flag == 1){
            return ServerResponse.fail("用户名已存在，请更换");
        }

        //2、设置不为空字段
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        user.setRole(Const.Role.ROLE_CUSTOMER);

        //2、MD5加密
        user.setPassword(MD5Util.MD5EncodeUtf8(user.getPassword()));
        flag = userMapper.insertSelective(user);

        if(flag == 0){
            return ServerResponse.fail("注册失败");
        }
        return ServerResponse.sucesss("注册成功");
    }

    @Override
    public ServerResponse<String> checkValid(String type, String content) {

        if(StringUtils.isNotBlank(type)){
            if(Const.USERNAME.equals(type)){
                int flag = userMapper.checkUserName(content);
                if(flag > 0){
                    return ServerResponse.fail("用户名已存在");
                }
            }
            if(Const.PASSWORD.equals(type)){
                if(StringUtils.isBlank(content)){
                    return ServerResponse.fail("密码不能为空");
                }
            }
        }else {
            return ServerResponse.fail("参数错误");
        }
        return ServerResponse.sucesss("校验成功");
    }

    @Override
    public ServerResponse<User> getUserInfo(String username) {
        User user = userMapper.getUserInfo(username);
        user.setPassword(StringUtils.EMPTY);
        return ServerResponse.sucesss(user);
    }

    @Override
    public ServerResponse<String> getForgetQuestion(String username) {

        //1、检查用户是否存在
        ServerResponse userIsExist = this.checkValid(Const.USERNAME , username);
        if(userIsExist.isSuccess()){
            return ServerResponse.fail("用户名不存在");
        }

        //2、查看用户名是否为空，以及问题是否为空
        if(StringUtils.isNotBlank(username)){
            String question = userMapper.getForgetQuestion(username);
            if(StringUtils.isNotBlank(question)){
                return ServerResponse.sucesss(question);
            }else {
                return ServerResponse.fail("该用户未设置找回密码问题");
            }
        }
        return ServerResponse.fail("未知错误");
    }

    @Override
    public ServerResponse<String> checkQuestionAnswer(String username, String question, String answer) {
        int resultCount = userMapper.getForgetAnswerByUsername(username,question,answer);
        if(resultCount > 0){
            String forgetToken = UUID.randomUUID().toString();
            TokenCache.setKey("token_"+username,forgetToken);
            return ServerResponse.sucesss(forgetToken);
        }
        return ServerResponse.fail("问题的答案不正确");
    }


}
