package com.zby.mmall.service;

import com.zby.mmall.common.ServerResponse;
import com.zby.mmall.pojo.User;
import org.springframework.stereotype.Service;

/**
 * @author beny
 * @Create 2020-09-19-17:50
 */
@Service
public interface UserService {

    ServerResponse<User> login(String username, String password);

    ServerResponse<String> register(User user);

    ServerResponse<String> checkValid(String type, String content);

    ServerResponse<User> getUserInfo(String username);

    ServerResponse<String> getForgetQuestion(String username);

    ServerResponse<String> checkQuestionAnswer(String username, String question, String answer);
}
