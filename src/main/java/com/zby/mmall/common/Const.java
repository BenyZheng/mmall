package com.zby.mmall.common;

/**
 * @author beny
 * @Create 2020-09-19-17:46
 */
public class Const {

    public static final String CURRENT_USER = "currentUser";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";

    public interface Role{
        int ROLE_CUSTOMER = 0;//普通用户
        int ROLE_ADMIN = 1;//管理员
    }
}
