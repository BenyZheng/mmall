package com.zby.mmall.common;

import com.sun.org.apache.regexp.internal.RE;
import lombok.Data;

import java.io.Serializable;

/**
 * @author beny
 * @Create 2020-09-16-22:59
 */
@Data
public class ServerResponse<T> implements Serializable {

    private int status;

    private String message;

    private T data;

    private ServerResponse(int status){
        this.status = status;
    }

    private ServerResponse(int status, T data){
        this.status = status;
        this.data = data;
    }

    private ServerResponse(int status , T data  ,String message){
        this.status = status;
        this.message = message;
        this.data = data;
    }

    private ServerResponse(int status , String message){
        this.status = status;
        this.message = message;
    }

    public boolean isSuccess(){
        return this.status == ResponseCode.SUCCESS.getCode();
    }

    public static <T> ServerResponse<T> sucesss(){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode(),ResponseCode.SUCCESS.getMessage());
    }

    public static <T> ServerResponse<T> sucesss(T data){
        return new ServerResponse<>(ResponseCode.SUCCESS.getCode() , data , ResponseCode.SUCCESS.getMessage());
    }

    public static <T> ServerResponse<T> fail(){
        return new ServerResponse<>(ResponseCode.ERROR.getCode() , ResponseCode.ERROR.getMessage());
    }

    public static <T> ServerResponse<T> fail(String message){
        return new ServerResponse<>(ResponseCode.ERROR.getCode(), message);
    }

    public static <T> ServerResponse<T> needLogin(){
        return new ServerResponse<>(ResponseCode.NEED_LOGIN.getCode(),ResponseCode.NEED_LOGIN.getMessage());
    }

    public static <T> ServerResponse<T> illegalArgument(){
        return new ServerResponse<>(ResponseCode.ILLEGAL_ARGUMENT.getCode() , ResponseCode.ILLEGAL_ARGUMENT.getMessage());
    }

}
