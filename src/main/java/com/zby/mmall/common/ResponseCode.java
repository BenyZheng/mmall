package com.zby.mmall.common;

import lombok.Data;

/**
 * @author beny
 * @Create 2020-09-16-22:59
 */

public enum ResponseCode {

    SUCCESS(0,"SUCESSS"),
    ERROR(1,"ERROR"),
    NEED_LOGIN(10,"NEDD_LOGIN"),
    ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT");

    private final int code;

    private final String message;

    ResponseCode(int code,String message){
        this.code = code;
        this.message = message;
    }

    public int getCode(){
        return this.code;
    }

    public String getMessage(){
        return this.message;
    }

}
